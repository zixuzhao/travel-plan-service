import { Factory, Seed } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';

import { Trip } from '../../api/models/Trip';

export class CreateTrips implements Seed {

    public async seed(factory: Factory, connection: Connection): Promise<Trip> {
        await factory(Trip)().seedMany(30);
    }
}
