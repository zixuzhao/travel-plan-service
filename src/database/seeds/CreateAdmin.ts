import { Connection } from 'typeorm';
import { Factory, Seed } from 'typeorm-seeding';
import { User } from '../../api/models/User';
import { Role } from '../../api/types/Role';

export class CreateAdmin implements Seed {

    public async seed(factory: Factory, connection: Connection): Promise<User> {

        const em = connection.createEntityManager();

        const now: Date = new Date();

        const user = new User();
        user.id = '5321fbe0-3615-11e9-b5ea-cf7ce68e7a00';
        user.firstName = 'admin';
        user.lastName = 'admin';
        user.email = 'admin@admin.com';
        user.username = 'admin';
        user.password = '123456';
        user.role = Role.ADMIN;
        user.createdAt = now;
        user.updatedAt = now;
        user.isDeleted = false;
        return await em.save(user);
    }
}
