import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTripTable1550765313993 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = new Table({
            name: 'trip',
            columns: [
                {
                    name: 'id',
                    type: 'varchar',
                    length: '255',
                    isPrimary: true,
                    isNullable: false,
                },
                {
                    name: 'country',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'state',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'city',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'address',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'zip_code',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                }, {
                    name: 'start_time',
                    type: 'timestamptz',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'end_time',
                    type: 'timestamptz',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'comment',
                    type: 'text',
                    isPrimary: false,
                    isNullable: true,
                },
                {
                    name: 'user_id',
                    type: 'varchar',
                    length: '255',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'created_at',
                    type: 'timestamptz',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'updated_at',
                    type: 'timestamptz',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'is_deleted',
                    type: 'boolean',
                    isPrimary: false,
                    isNullable: false,
                },
                {
                    name: 'deleted_at',
                    type: 'timestamptz',
                    isPrimary: false,
                    isNullable: true,
                },
            ],
        });
        await queryRunner.createTable(table);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('trip');
    }

}
