import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import * as uuid from 'uuid';

import { Trip } from '../../api/models/Trip';

define(Trip, (faker: typeof Faker) => {
    const country = faker.address.country();
    const state = faker.address.state();
    const city = faker.address.city();
    const address = faker.address.streetAddress();
    const zipCode = faker.address.zipCode();
    const startTime = new Date();
    startTime.setDate(startTime.getDate() + 2);
    const endTime = new Date();
    endTime.setDate(endTime.getDate() + 2);
    const comment = faker.random.words(3);

    const now: Date = new Date();

    const trip = new Trip();
    trip.id = uuid.v1();
    trip.country = country;
    trip.state = state;
    trip.city = city;
    trip.address = address;
    trip.zipCode = zipCode;
    trip.startTime = startTime;
    trip.endTime = endTime;
    trip.comment = comment;
    trip.userId = '5321fbe0-3615-11e9-b5ea-cf7ce68e7a00';
    trip.createdAt = now;
    trip.updatedAt = now;
    trip.isDeleted = false;
    return trip;
});
