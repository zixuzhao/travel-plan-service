import { User } from '../User';

export class Jwt {
    public user: User;
    public token: string;
}
