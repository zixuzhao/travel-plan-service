import { EntityRepository, getManager, Repository } from 'typeorm';

import { User } from '../models/User';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    public async safeDelete(id: string): Promise<User> {
        const user: User = await this.findOne({id});
        const current = new Date();
        const deleteSuffix = `_deleted_at_${current.valueOf()}`;

        user.email = user.email + deleteSuffix;
        user.username = user.username + deleteSuffix;
        user.deletedAt = current;
        user.isDeleted = true;
        return this.save(user);
    }

    public async supervisorSearch(firstName: string = '',
                                  lastName: string = '',
                                  email: string = '',
                                  username: string = '',
                                  role: string = '',
                                  size: number = 10,
                                  page: number = 1): Promise<{ data: User[], count: number }> {
        const promiseQuery = getManager()
            .createQueryBuilder(User, 'user')
            .where('user.isDeleted = false')
            .andWhere('user.firstName ILIKE :firstName', {firstName: '%' + firstName + '%'})
            .andWhere('user.lastName ILIKE :lastName', {lastName: '%' + lastName + '%'})
            .andWhere('user.email ILIKE :email', {email: '%' + email + '%'})
            .andWhere('user.username ILIKE :username', {username: '%' + username + '%'})
            .andWhere('user.role ILIKE :role', {role: '%' + role + '%'})
            .orderBy('LOWER(user.username)').limit(size).offset(size * (page - 1));
        return {
            data: await promiseQuery.getMany(),
            count: await promiseQuery.getCount(),
        };
    }

    public async userSearch(firstName: string = '',
                            lastName: string = '',
                            email: string = '',
                            username: string = '',
                            role: string = '',
                            size: number = 10,
                            page: number = 1): Promise<{ data: User[], count: number }> {
        const promiseQuery = getManager()
            .createQueryBuilder(User, 'user')
            .where('user.isDeleted = false')
            .andWhere('user.firstName ILIKE :firstName', {firstName: '%' + firstName + '%'})
            .andWhere('user.lastName ILIKE :lastName', {lastName: '%' + lastName + '%'})
            .andWhere('user.email ILIKE :email', {email: '%' + email + '%'})
            .andWhere('user.username ILIKE :username', {username: '%' + username + '%'})
            .andWhere('user.role ILIKE :role', {role: '%' + role + '%'})
            .andWhere('user.role != :admin', {admin: 'ADMIN'})
            .orderBy('LOWER(user.username)').limit(size).offset(size * (page - 1));
        return {
            data: await promiseQuery.getMany(),
            count: await promiseQuery.getCount(),
        };
    }
}
