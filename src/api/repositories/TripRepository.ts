import { EntityRepository, getManager, Repository } from 'typeorm';

import { Trip } from '../models/Trip';
import moment from 'moment';

@EntityRepository(Trip)
export class TripRepository extends Repository<Trip> {

    public async safeDelete(id: string): Promise<Trip> {
        const trip: Trip = await this.findOne({ id });
        trip.deletedAt = new Date();
        trip.isDeleted = true;
        return this.save(trip);
    }

    public async safeDeleteByUserId(userId: string): Promise<Trip[]> {
        const trips: Trip[] = await this.find({ where: { userId } });
        trips.forEach((trip) => {
            trip.deletedAt = new Date();
            trip.isDeleted = true;
        });
        return this.save(trips);
    }

    public async searchTripsByUID(uid:       string = '',
                                  country:   string = '',
                                  state:     string = '',
                                  city:      string = '',
                                  address:   string = '',
                                  zipCode:   string = '',
                                  startTime: string,
                                  endTime:   string,
                                  size:      number,
                                  page:      number,
                                  future:    boolean): Promise<{ data: Trip[], count: number }> {
        const queryBuilder = await getManager().createQueryBuilder(Trip, 'trip');
        queryBuilder
            .where('trip.userId = :uid', { uid })
            .andWhere('trip.isDeleted = false')
            .andWhere('trip.country ILIKE :country', { country: '%' + country + '%' })
            .andWhere('trip.state ILIKE :state', { state: '%' + state + '%' })
            .andWhere('trip.city ILIKE :city', { city: '%' + city + '%' })
            .andWhere('trip.address ILIKE :address', { address: '%' + address + '%' })
            .andWhere('trip.zipCode ILIKE :zipCode', { zipCode: '%' + zipCode + '%' });
        if (startTime !== undefined) {
            const iniOne = moment(startTime, 'M/DD/YYYY').format('YYYY-MM-DD');
            const endOne = moment(startTime, 'M/DD/YYYY').add(1, 'day').format('YYYY-MM-DD');
            queryBuilder
                .andWhere('trip.start_time >= :iniOne AND trip.start_time < :endOne', { iniOne, endOne });
        }

        if (endTime !== undefined) {
            const iniTwo = moment(endTime, 'M/DD/YYYY').format('YYYY-MM-DD');
            const endTwo = moment(endTime, 'M/DD/YYYY').add(1, 'day').format('YYYY-MM-DD');
            queryBuilder
                .andWhere('trip.end_time >= :iniTwo AND trip.end_time < :endTwo', { iniTwo, endTwo });
        }
        if (future) {
            const ini = moment().format('YYYY-MM-DD');
            queryBuilder.andWhere('trip.start_time >= :ini', { ini });
        }
        queryBuilder.orderBy('trip.startTime', 'ASC').limit(size).offset(size * (page - 1));
        return {
            data: await queryBuilder.getMany(),
            count: await queryBuilder.getCount(),
        };
    }

    public async searchTrips(country:   string = '',
                             state:     string = '',
                             city:      string = '',
                             address:   string = '',
                             zipCode:   string = '',
                             startTime: string,
                             endTime:   string,
                             email:     string = '',
                             size:      number,
                             page:      number,
                             future:    boolean): Promise<{ data: Trip[], count: number }> {
        const queryBuilder = await getManager().createQueryBuilder(Trip, 'trip');
        queryBuilder
            .where('trip.isDeleted = false')
            .andWhere('trip.country ILIKE :country', { country: '%' + country + '%' })
            .andWhere('trip.state ILIKE :state', { state: '%' + state + '%' })
            .andWhere('trip.city ILIKE :city', { city: '%' + city + '%' })
            .andWhere('trip.address ILIKE :address', { address: '%' + address + '%' })
            .andWhere('trip.zipCode ILIKE :zipCode', { zipCode: '%' + zipCode + '%' });
        if (startTime !== undefined) {
            const iniOne = moment(startTime, 'M/DD/YYYY').format('YYYY-MM-DD');
            const endOne = moment(startTime, 'M/DD/YYYY').add(1, 'day').format('YYYY-MM-DD');
            queryBuilder
                .andWhere('trip.start_time >= :iniOne AND trip.start_time < :endOne', { iniOne, endOne });
        }

        if (endTime !== undefined) {
            const iniTwo = moment(endTime, 'M/DD/YYYY').format('YYYY-MM-DD');
            const endTwo = moment(endTime, 'M/DD/YYYY').add(1, 'day').format('YYYY-MM-DD');
            queryBuilder
                .andWhere('trip.end_time >= :iniTwo AND trip.end_time < :endTwo', { iniTwo, endTwo });
        }
        queryBuilder.leftJoinAndSelect('trip.user', 'user')
            .andWhere('user.isDeleted = false')
            .andWhere('user.email ILIKE :email', { email: '%' + email + '%' });
        queryBuilder.orderBy('trip.startTime', 'ASC').limit(size).offset(size * (page - 1));

        if (future) {
            const ini = moment().format('YYYY-MM-DD');
            queryBuilder.andWhere('trip.start_time >= :ini', { ini });
        }
        return {
            data: await queryBuilder.getMany(),
            count: await queryBuilder.getCount(),
        };
    }

    public async getTripsReport(offset: number): Promise<Trip[]> {
        const queryBuilder = await getManager().createQueryBuilder(Trip, 'trip');
        queryBuilder
            .leftJoinAndSelect('trip.user', 'user')
            .where('user.isDeleted = false')
            .andWhere('trip.isDeleted = false');
        const nextMonth = moment().utcOffset(0 - offset).add(1, 'month');
        const ini = nextMonth.format('YYYY-MM') + '-01';
        const end = nextMonth.format('YYYY-MM') + `-${nextMonth.daysInMonth()}`;
        queryBuilder
            .andWhere('trip.start_time >= :ini AND trip.start_time < :end', {ini, end});
        queryBuilder.orderBy('trip.startTime', 'ASC');
        return  await queryBuilder.getMany();
    }

    public async getTripsReportByUID(uid: string, offset: number): Promise<Trip[]> {
        const queryBuilder = await getManager().createQueryBuilder(Trip, 'trip');
        queryBuilder
            .leftJoinAndSelect('trip.user', 'user')
            .where('user.isDeleted = false')
            .andWhere('trip.userId = :uid', { uid })
            .andWhere('trip.isDeleted = false');
        const nextMonth = moment().utcOffset(0 - offset).add(1, 'month');
        const ini = nextMonth.format('YYYY-MM') + '-01';
        const end = nextMonth.format('YYYY-MM') + `-${nextMonth.daysInMonth()}`;
        queryBuilder
            .andWhere('trip.start_time >= :ini AND trip.start_time < :end', {ini, end});
        queryBuilder.orderBy('trip.startTime', 'ASC');
        return  await queryBuilder.getMany();
    }
}
