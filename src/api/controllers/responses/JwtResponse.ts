import { User } from '../../types/User';

export class JwtResponse {

    public success: boolean;
    public message: string;
    public user: User;
    public token: string;

    constructor(success: boolean, message: string, user: User, token: string) {
        this.success = success;
        this.message = message;
        this.user = user;
        this.token = token;
    }
}
