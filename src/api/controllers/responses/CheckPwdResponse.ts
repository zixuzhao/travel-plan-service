
export class CheckPwdResponse {

    public success: boolean;
    public message: string;
    public data: boolean;

    constructor(success: boolean, message: string, data: boolean) {
        this.success = success;
        this.message = message;
        this.data = data;
    }
}
