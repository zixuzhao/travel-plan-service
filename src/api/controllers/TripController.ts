import { Authorized, Body, Delete, Get, JsonController, OnUndefined, Param, Post, Put, QueryParam, Req } from 'routing-controllers';
import { InfoNotFoundError } from '../errors/InfoNotFoundError';
import { TripService } from '../services/TripService';
import { Trip } from '../models/Trip';

@Authorized()
@JsonController('/trips')
export class TripController {

    constructor(
        private tripService: TripService
    ) {
    }

    /* User self operations */
    @Get('/self/fetch/all')
    @OnUndefined(InfoNotFoundError)
    public fetchAllSelf(@Req() req: any,
                        @QueryParam('country') country: string,
                        @QueryParam('state') state: string,
                        @QueryParam('city') city: string,
                        @QueryParam('address') address: string,
                        @QueryParam('zipCode') zipCode: string,
                        @QueryParam('startTime') startTime: string,
                        @QueryParam('endTime') endTime: string,
                        @QueryParam('size') size: number,
                        @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.tripService.findAllTripsByUID(req.user.id, country, state, city, address, zipCode,
            startTime, endTime, size, page);
    }

    @Get('/self/fetch/future')
    @OnUndefined(InfoNotFoundError)
    public fetchFutureSelf(@Req() req: any,
                           @QueryParam('country') country: string,
                           @QueryParam('state') state: string,
                           @QueryParam('city') city: string,
                           @QueryParam('address') address: string,
                           @QueryParam('zipCode') zipCode: string,
                           @QueryParam('startTime') startTime: string,
                           @QueryParam('endTime') endTime: string,
                           @QueryParam('size') size: number,
                           @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.tripService.findFutureTripsByUID(req.user.id, country, state, city, address, zipCode,
            startTime, endTime, size, page);
    }

    @Post('/self/create')
    @OnUndefined(InfoNotFoundError)
    public selfCreate(@Req() req: any, @Body() trip: Trip): Promise<Trip | undefined> {
        return this.tripService.selfCreate(req.user.id, trip);
    }

    @Put('/self/update/:id')
    @OnUndefined(InfoNotFoundError)
    public selfUpdate(@Req() req: any,
                      @Param('id') id: string,
                      @Body() trip: Trip): Promise<Trip | undefined> {
        return this.tripService.selfUpdate(req.user.id, id, trip);
    }

    @Delete('/self/delete/:id')
    @OnUndefined(InfoNotFoundError)
    public selfDelete(@Req() req: any, @Param('id') id: string): Promise<string | undefined> {
        return this.tripService.selfDelete(req.user.id, id);
    }

    @Get('/self/report/:offset')
    public async generateReport(@Req() req: any,
                                @QueryParam('offset') offset: number): Promise<Trip[]> {
        return await this.tripService.tripsReportGeneratorByUID(req.user.id, offset);
    }
}
