import { Authorized, Body, Delete, Get, JsonController, OnUndefined, Param, Post, Put, QueryParam, Req } from 'routing-controllers';

import { InfoNotFoundError } from '../errors/InfoNotFoundError';
import { User } from '../models/User';
import { UserService } from '../services/UserService';
import { Role } from '../types/Role';

@JsonController('/users')
export class UserController {

    constructor(
        private userService: UserService
    ) {
    }

    @Authorized([Role.ADMIN, Role.MANAGER])
    @Get('/all')
    public find(@Req() req: any,
                @QueryParam('firstName') firstName: string,
                @QueryParam('lastName') lastName: string,
                @QueryParam('email') email: string,
                @QueryParam('username') username: string,
                @QueryParam('role') role: string,
                @QueryParam('size') size: number,
                @QueryParam('page') page: number): Promise<{ data: User[], count: number }> {
        if (req.user.role === Role.ADMIN) {
            return this.userService.supervisorSearch(firstName, lastName, email, username, role, size, page);
        } else {
            return this.userService.userSearch(firstName, lastName, email, username, role, size, page);
        }
    }

    @Authorized([Role.ADMIN, Role.MANAGER])
    @Get('/search/:id')
    @OnUndefined(InfoNotFoundError)
    public one(@Param('id') id: string): Promise<User | undefined> {
        return this.userService.findOne(id);
    }

    @Authorized([Role.ADMIN, Role.MANAGER])
    @Post('/create')
    public create(@Body() user: User): Promise<User> {
        return this.userService.create(user);
    }

    @Authorized([Role.ADMIN, Role.MANAGER])
    @Put('/update/:id')
    @OnUndefined(InfoNotFoundError)
    public update(@Param('id') id: string, @Body() user: User): Promise<User | undefined> {
        return this.userService.update(id, user);
    }

    @Authorized([Role.ADMIN, Role.MANAGER])
    @Delete('/truncate/:id')
    public delete(@Param('id') id: string): Promise<string> {
        return this.userService.truncate(id);
    }

    /* User self operations */

    /* @Deprecated Service */
    @Authorized()
    @Get('/self/fetch')
    @OnUndefined(InfoNotFoundError)
    public fetchSelf(@Req() req: any): Promise<User | undefined> {
        return this.userService.findOneWithTrips(req.user.id);
    }

    @Authorized()
    @Put('/self/update')
    @OnUndefined(InfoNotFoundError)
    public updateSelf(@Req() req: any, @Body() user: User): Promise<User | undefined> {
        return this.userService.update(req.user.id, user);
    }

    @Authorized()
    @Delete('/self/delete')
    @OnUndefined(InfoNotFoundError)
    public deleteSelf(@Req() req: any): Promise<string> {
        return this.userService.delete(req.user.id);
    }

    @Authorized()
    @Delete('/self/truncate')
    @OnUndefined(InfoNotFoundError)
    public truncateSelf(@Req() req: any): Promise<string> {
        return this.userService.truncate(req.user.id);
    }

}
