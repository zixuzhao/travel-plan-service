import { Authorized, Body, JsonController, OnUndefined, Post } from 'routing-controllers';
import { User } from '../models/User';
import { UserService } from '../services/UserService';
import { AuthService } from '../services/AuthService';
import { JwtResponse } from './responses/JwtResponse';
import { InfoNotFoundError } from '../errors/InfoNotFoundError';
import { CheckPwdResponse } from './responses/CheckPwdResponse';

@JsonController('/auth')
export class AuthController {
    private ALLGOOD = 'ALL GOOD';

    constructor(
        private userService: UserService,
        private authService: AuthService
    ) {
    }

    @Post('/register')
    public register(@Body() user: User): Promise<User> {
        return this.userService.create(user);
    }

    @Post('/login')
    @OnUndefined(InfoNotFoundError)
    public async login(@Body() pair: { username, password }): Promise<JwtResponse | undefined> {
        const jwt = await this.authService.generateCredential(pair.username, pair.password);
        if (jwt === undefined) {
            return undefined;
        }
        return new JwtResponse(true, this.ALLGOOD, jwt.user, jwt.token);
    }

    @Authorized()
    @Post('/validate')
    @OnUndefined(InfoNotFoundError)
    public async auth(@Body() pair: { id, password }): Promise<CheckPwdResponse | undefined> {
        const res = await this.authService.validatePassword(pair.id, pair.password);
        if (res) {
            return new CheckPwdResponse(true, this.ALLGOOD, res);
        } else {
            return new CheckPwdResponse(false, 'Authentication Failed', res);
        }
    }
}
