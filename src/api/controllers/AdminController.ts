import { Authorized, Body, Delete, Get, JsonController, OnUndefined, Param, Post, Put, QueryParam, Req } from 'routing-controllers';
import { User } from '../models/User';
import { Role } from '../types/Role';
import { AdminService } from '../services/AdminService';
import { InfoNotFoundError } from '../errors/InfoNotFoundError';
import { Trip } from '../models/Trip';

@Authorized([Role.ADMIN])
@JsonController('/admin')
export class AdminController {

    constructor(
        private adminService: AdminService
    ) {
    }

    /* Admin User API */
    @Put('/user/update/:id')
    @OnUndefined(InfoNotFoundError)
    public update(@Param('id') id: string, @Body() user: User): Promise<User | undefined> {
        return this.adminService.updateUser(id, user);
    }

    /* Admin Trip API for searching */
    @Get('/trip/all')
    @OnUndefined(InfoNotFoundError)
    public allTrips(@QueryParam('country') country: string,
                    @QueryParam('state') state: string,
                    @QueryParam('city') city: string,
                    @QueryParam('address') address: string,
                    @QueryParam('zipCode') zipCode: string,
                    @QueryParam('startTime') startTime: string,
                    @QueryParam('endTime') endTime: string,
                    @QueryParam('email') email: string,
                    @QueryParam('size') size: number,
                    @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.adminService.findAllTrips(country, state, city, address, zipCode, startTime,
                                              endTime, email, size, page);
    }

    @Get('/trip/future')
    @OnUndefined(InfoNotFoundError)
    public futureTrips(@QueryParam('country') country: string,
                       @QueryParam('state') state: string,
                       @QueryParam('city') city: string,
                       @QueryParam('address') address: string,
                       @QueryParam('zipCode') zipCode: string,
                       @QueryParam('startTime') startTime: string,
                       @QueryParam('endTime') endTime: string,
                       @QueryParam('email') email: string,
                       @QueryParam('size') size: number,
                       @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.adminService.findFutureTrips(country, state, city, address, zipCode, startTime,
                                                 endTime, email, size, page);
    }

    @Get('/trip/user/all/:uid')
    @OnUndefined(InfoNotFoundError)
    public findAllTripsByUID(@Param('uid') uid: string,
                             @QueryParam('country') country: string,
                             @QueryParam('state') state: string,
                             @QueryParam('city') city: string,
                             @QueryParam('address') address: string,
                             @QueryParam('zipCode') zipCode: string,
                             @QueryParam('startTime') startTime: string,
                             @QueryParam('endTime') endTime: string,
                             @QueryParam('size') size: number,
                             @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.adminService.findAllTripsByUID(uid, country, state, city, address, zipCode,
                                                   startTime, endTime, size, page);
    }

    @Get('/trip/user/future/:uid')
    @OnUndefined(InfoNotFoundError)
    public findFutureTripsByUID(@Param('uid') uid: string,
                                @QueryParam('country') country: string,
                                @QueryParam('state') state: string,
                                @QueryParam('city') city: string,
                                @QueryParam('address') address: string,
                                @QueryParam('zipCode') zipCode: string,
                                @QueryParam('startTime') startTime: string,
                                @QueryParam('endTime') endTime: string,
                                @QueryParam('size') size: number,
                                @QueryParam('page') page: number): Promise<{ data: Trip[], count: number }> {
        return this.adminService.findFutureTripsByUID(uid, country, state, city, address, zipCode,
                                                      startTime, endTime, size, page);
    }

    /* Admin Trip API for a specific user */
    @Post('/trip/create')
    @OnUndefined(InfoNotFoundError)
    public createTrip(@Body() trip: Trip): Promise<Trip | undefined> {
        return this.adminService.createTrip(trip);
    }

    @Put('/trip/update/:id')
    @OnUndefined(InfoNotFoundError)
    public updateTrip(@Param('id') id: string,
                      @Body() trip: Trip): Promise<Trip | undefined> {
        return this.adminService.updateTrip(id, trip);
    }

    @Delete('/trip/delete/:id')
    @OnUndefined(InfoNotFoundError)
    public deleteTrip(@Req() req: any, @Param('id') id: string): Promise<string | undefined> {
        return this.adminService.deleteTrip(id);
    }

    @Get('/trip/report/all/:offset')
    public async generateReport(@QueryParam('offset') offset: number): Promise<Trip[]> {
        return await this.adminService.tripsReportGenerator(offset);
    }

    @Get('/trip/report/user/:id/:offset')
    public async generateReportByUID(@Param('id') id: string,
                                     @QueryParam('offset') offset: number): Promise<Trip[]> {
        return await this.adminService.tripsReportGeneratorByUID(id, offset);
    }
}
