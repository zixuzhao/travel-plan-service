/**
 * events
 * ---------------------
 * Define all your possible custom events here.
 */
export const events = {
    user: {
        created: 'onUserCreate',
        updated: 'onUserUpdate',
    },
    trip: {
        created: 'onTripCreate',
        updated: 'onTripUpdate',
    },
};
