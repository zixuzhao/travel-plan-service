import { HttpError } from 'routing-controllers';

export class InfoNotFoundError extends HttpError {
    constructor() {
        super(404, 'Info not found!');
    }
}
