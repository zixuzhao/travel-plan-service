import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Role } from '../types/Role';
import { Trip } from './Trip';

@Entity()
export class User {

    public static hashPassword(password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    }

    public static comparePassword(user: User, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                resolve(res === true);
            });
        });
    }

    @PrimaryColumn('uuid')
    public id: string;

    @IsNotEmpty()
    @Column({ name: 'first_name' })
    public firstName: string;

    @IsNotEmpty()
    @Column({ name: 'last_name' })
    public lastName: string;

    @IsNotEmpty()
    @Column()
    public email: string;

    @IsNotEmpty()
    @Column()
    public username: string;

    @IsNotEmpty()
    @Column()
    @Exclude({ toPlainOnly: true })
    public password: string;

    @Column()
    public role: string;

    @OneToMany(type => Trip, trip => trip.user)
    public trips: Trip[];

    @Column({ name: 'created_at' })
    @Exclude({toClassOnly: true})
    public createdAt: Date;

    @Column({ name: 'updated_at' })
    @Exclude({toClassOnly: true})
    public updatedAt: Date;

    @Column({ name: 'is_deleted' })
    @Exclude()
    public isDeleted: boolean;

    @Column({ name: 'deleted_at' })
    @Exclude()
    public deletedAt: Date;

    public toString(): string {
        return `${this.firstName} ${this.lastName} (${this.email})`;
    }

    @BeforeInsert()
    @BeforeUpdate()
    public async hashPassword(): Promise<void> {
        if (this.password !== undefined && this.password !== '') {
            this.password = await User.hashPassword(this.password);
        }
    }

    @BeforeInsert()
    public async setDefaults(): Promise<void> {
        this.role = this.role === undefined ? Role.REGULAR : this.role;
        const current: Date = new Date();
        this.createdAt = current;
        this.updatedAt = current;
        this.isDeleted = false;
    }

    @BeforeUpdate()
    public async updateTimestamp(): Promise<void> {
        this.updatedAt = new Date();
    }
}
