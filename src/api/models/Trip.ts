import { IsNotEmpty } from 'class-validator';
import { Exclude } from 'class-transformer';
import { BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { User } from './User';

@Entity()
export class Trip {

    @PrimaryColumn('uuid')
    public id: string;

    @IsNotEmpty()
    @Column()
    public country: string;

    @Column()
    public state: string;

    @IsNotEmpty()
    @Column()
    public city: string;

    @Column()
    public address: string;

    @Column({name: 'zip_code'})
    public zipCode: string;

    @IsNotEmpty()
    @Column({name: 'start_time'})
    public startTime: Date;

    @IsNotEmpty()
    @Column({name: 'end_time'})
    public endTime: Date;

    @Column()
    public comment: string;

    @IsNotEmpty()
    @Column({
        name: 'user_id',
    })
    public userId: string;

    @ManyToOne(type => User, user => user.trips)
    @JoinColumn({name: 'user_id'})
    public user: User;

    @Column({name: 'created_at'})
    @Exclude({toClassOnly: true})
    public createdAt: Date;

    @Column({name: 'updated_at'})
    @Exclude({toClassOnly: true})
    public updatedAt: Date;

    @Column({name: 'is_deleted'})
    @Exclude()
    public isDeleted: boolean;

    @Column({name: 'deleted_at'})
    @Exclude()
    public deletedAt: Date;

    public toString(): string {
        return `user: ${this.userId}, trip: ${this.id}`;
    }

    @BeforeInsert()
    public async setDefaults(): Promise<void> {
        const current: Date = new Date();
        this.createdAt = current;
        this.updatedAt = current;
        this.isDeleted = false;
    }

    @BeforeUpdate()
    public async updateTimestamp(): Promise<void> {
        this.updatedAt = new Date();
    }
}
