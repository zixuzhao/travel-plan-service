
export class TimeUtilities {

    constructor(
        private time: Date
    ) {
        this.time = time;
    }

    public toUnixInMicroSeconds(): number {
        return this.time.valueOf() * 1000;
    }

    public toUnixInMillionSeconds(): number {
        return this.time.valueOf();
    }

    public toUnixInSeconds(): number {
        return Math.trunc(this.time.valueOf() / 1000);
    }
}
