import { Trip } from '../models/Trip';
import moment from 'moment';

export class ReportGenerator {

    public generateReportJson(data: Trip[], offset: number): object {
        const template = this.getTemplate();
        data.forEach(e => {
            const startTime = moment(e.startTime).utcOffset(0 - offset).startOf('day').format('M/DD/YYYY');
            const endTime = moment(e.endTime).utcOffset(0 - offset).startOf('day').format('M/DD/YYYY');
            const record = [e.daysLeft, e.country, e.state, e.city, e.address, e.zipCode, startTime, endTime];
            template.content[1].table.body.push(record);
        });
        return template;
    }

    private getTemplate(): any {
        return {
            pageOrientation: 'landscape',
            content: [
                {text: 'Travel Plan for Next Month', style: 'header'},
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        widths: [70, 75, 75, 75, 130, 75, 75, 75],
                        body: [
                            [
                                {text: ''},
                                {text: 'Country', style: 'tbHeader'},
                                {text: 'State', style: 'tbHeader'},
                                {text: 'City', style: 'tbHeader'},
                                {text: 'Address', style: 'tbHeader'},
                                {text: 'Zip Code', style: 'tbHeader'},
                                {text: 'Start Date', style: 'tbHeader'},
                                {text: 'End Date', style: 'tbHeader'}],
                        ],
                    },
                    layout: 'lightHorizontalLines',
                },
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10],
                },
                tableExample: {
                    margin: [0, 5, 0, 15],
                },
                tbHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black',
                },
            },
            defaultStyle: {
                // alignment: 'justify'
            },
        };
    }
}
