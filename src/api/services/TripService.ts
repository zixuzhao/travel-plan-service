import { Service } from 'typedi';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { Trip } from '../models/Trip';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { TripRepository } from '../repositories/TripRepository';
import uuid from 'uuid';
import { events } from '../subscribers/events';
import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';

@Service()
export class TripService {

    private static updateAttributes(old: Trip, updated: Trip): Trip {
        for (const key in old) {
            if (updated[key] !== undefined && key !== 'id') {
                old[key] = updated[key];
            }
        }
        return old;
    }

    constructor(
        @Logger(__filename) private log: LoggerInterface,
        @OrmRepository() private tripRepository: TripRepository,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface
    ) {
    }

    public async findAllTripsByUID(uid:       string,
                                   country:   string,
                                   state:     string,
                                   city:      string,
                                   address:   string,
                                   zipCode:   string,
                                   startTime: string,
                                   endTime:   string,
                                   size:      number,
                                   page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all users with parameters');
        return await this.tripRepository.searchTripsByUID(uid, country, state, city, address, zipCode,
                                                          startTime, endTime, size, page, false);
    }

    public async findFutureTripsByUID(uid:       string,
                                      country:   string,
                                      state:     string,
                                      city:      string,
                                      address:   string,
                                      zipCode:   string,
                                      startTime: string,
                                      endTime:   string,
                                      size:      number,
                                      page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all trips with parameters');
        return await this.tripRepository.searchTripsByUID(uid, country, state, city, address, zipCode,
                                                          startTime, endTime, size, page, true);
    }

    public async selfCreate(userId: string, trip: Trip): Promise<Trip | undefined> {
        this.log.info('Create a new trip for user self => ', trip.toString());

        if (trip.userId !== undefined && userId !== trip.userId) {
            this.log.warn(`Non Admin user ${userId} is trying to create a trip for others`);
            return undefined;
        } else {
            trip.id = uuid.v1();
            if (trip.userId === undefined) {
                trip.userId = userId;
            }
            const created = await this.tripRepository.save(trip);
            this.eventDispatcher.dispatch(events.user.created, created);
            return created;
        }
    }

    public async selfUpdate(userId: string, id: string, trip: Trip): Promise<Trip | undefined> {
        this.log.info('Updating a new trip for user self=> ', trip.toString());

        const current = await this.tripRepository.findOne({id, isDeleted: false});
        if (current === undefined) {
            /* A ghost user cannot update itself */
            return undefined;
        }
        if (current.userId !== userId) {
            this.log.warn(`User ${userId} is trying to update a trip for others`);
            return undefined;
        }
        const candidate = TripService.updateAttributes(current, trip);
        const updated = await this.tripRepository.save(candidate);
        this.eventDispatcher.dispatch(events.trip.updated, updated);
        return updated;
    }

    public async selfDelete(userId: string, id: string): Promise<string> {
        this.log.info('Delete a trip for user self');
        const current = await this.tripRepository.findOne({id, isDeleted: false});
        if (current !== undefined && current.userId !== userId ) {
            this.log.warn(`User ${userId} is trying to delete a trip for others`);
            return undefined;
        }
        await this.tripRepository.safeDelete(id);
        return id;
    }

    public async tripsReportGeneratorByUID(id: string, offset: number): Promise<Trip[]> {
        return await this.tripRepository.getTripsReportByUID(id, offset);
    }
}
