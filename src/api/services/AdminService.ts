import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { User } from '../models/User';
import { UserRepository } from '../repositories/UserRepository';
import { TripRepository } from '../repositories/TripRepository';
import { events } from '../subscribers/events';
import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';
import { Role } from '../types/Role';
import { Trip } from '../models/Trip';
import uuid from 'uuid';

@Service()
export class AdminService {

    private static updateAttributes<T>(old: T, updated: T): T {
        for (const key in old) {
            if (updated[key] !== undefined && old.hasOwnProperty(key) && key !== 'id' ) {
                old[key] = updated[key];
            }
        }
        return old;
    }

    constructor(
        @OrmRepository() private userRepository: UserRepository,
        @OrmRepository() private tripRepository: TripRepository,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
        @Logger(__filename) private log: LoggerInterface
    ) {
    }

    public async findAllTripsByUID(uid:       string,
                                   country:   string,
                                   state:     string,
                                   city:      string,
                                   address:   string,
                                   zipCode:   string,
                                   startTime: string,
                                   endTime:   string,
                                   size:      number,
                                   page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all users with parameters');
        return this.tripRepository.searchTripsByUID(uid, country, state, city, address, zipCode,
                                                    startTime, endTime, size, page, false);
    }

    public async findFutureTripsByUID(uid:       string,
                                      country:   string,
                                      state:     string,
                                      city:      string,
                                      address:   string,
                                      zipCode:   string,
                                      startTime: string,
                                      endTime:   string,
                                      size:      number,
                                      page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all users with parameters');
        return this.tripRepository.searchTripsByUID(uid, country, state, city, address, zipCode,
                                                    startTime, endTime, size, page, true);
    }

    public async updateUser(id: string, user: User): Promise<User | undefined> {
        this.log.info('Update a user with id: ', id);
        const current = await this.userRepository.findOne({ id, isDeleted: false });
        if (current === undefined) {
            /* Cannot find a user */
            return current;
        }
        if (!(user.role in Role)) {
            /* User role should be in the role scope */
            throw Error('Receiving invalid user role ');
        }
        if (user.password === undefined || user.password === '') {
            /* prevent ORM recalculate password based on a hash */
            current.password = user.password;
        }
        const candidate = AdminService.updateAttributes(current, user);
        const updated = await this.userRepository.save(candidate);
        this.eventDispatcher.dispatch(events.user.updated, updated);
        return updated;
    }

    /* Trips service for admin users only */

    public async findAllTrips(country:   string,
                              state:     string,
                              city:      string,
                              address:   string,
                              zipCode:   string,
                              startTime: string,
                              endTime:   string,
                              email:     string,
                              size:      number,
                              page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all users with parameters');
        return this.tripRepository.searchTrips(country, state, city, address, zipCode,
                                               startTime, endTime, email, size, page, false);
    }

    public async findFutureTrips(country:   string,
                                 state:     string,
                                 city:      string,
                                 address:   string,
                                 zipCode:   string,
                                 startTime: string,
                                 endTime:   string,
                                 email:     string,
                                 size:      number,
                                 page:      number): Promise<{ data: Trip[], count: number }> {
        this.log.info('Find all users with parameters');
        return this.tripRepository.searchTrips(country, state, city, address, zipCode,
                                               startTime, endTime, email, size, page, true);
    }

    public async createTrip(trip: Trip): Promise<Trip> {
        this.log.info('Create a new trip => ', trip.toString());
        trip.id = uuid.v1();
        const created = await this.tripRepository.save(trip);
        this.eventDispatcher.dispatch(events.user.created, created);
        return created;
    }

    public async updateTrip(id: string, trip: Trip): Promise<Trip> {
        this.log.info('Updating a new trip => ', trip.toString());
        const current = await this.tripRepository.findOne({id, isDeleted: false});
        if (current === undefined) {
            return current;
        }
        const candidate = AdminService.updateAttributes(current, trip);
        const updated = await this.tripRepository.save(candidate);
        this.eventDispatcher.dispatch(events.trip.updated, updated);
        return updated;
    }

    public async deleteTrip(id: string): Promise<string> {
        this.log.info('Delete a trip');
        await this.tripRepository.safeDelete(id);
        return id;

    }

    public async tripsReportGenerator(offset: number): Promise<Trip[]> {
        return await this.tripRepository.getTripsReport(offset);
    }

    public async tripsReportGeneratorByUID(id: string, offset: number): Promise<Trip[]> {
        return await this.tripRepository.getTripsReportByUID(id, offset);
    }
}
