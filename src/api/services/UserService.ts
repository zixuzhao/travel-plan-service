import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import uuid from 'uuid';

import { EventDispatcher, EventDispatcherInterface } from '../../decorators/EventDispatcher';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { User } from '../models/User';
import { UserRepository } from '../repositories/UserRepository';
import { events } from '../subscribers/events';
import { TripRepository } from '../repositories/TripRepository';
import { Role } from '../types/Role';

@Service()
export class UserService {

    private static updateAttributes(old: User, updated: User): User {
        for (const key in old) {
            if (updated[key] !== undefined && key !== 'id') {
                old[key] = updated[key];
            }
        }
        return old;
    }

    constructor(
        @OrmRepository() private userRepository: UserRepository,
        @OrmRepository() private tripRepository: TripRepository,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
        @Logger(__filename) private log: LoggerInterface
    ) {
    }

    public async supervisorSearch(firstName: string,
                                  lastName:  string,
                                  email:     string,
                                  username:  string,
                                  role:      string,
                                  size:      number,
                                  page:      number): Promise<{ data: User[], count: number }> {
        this.log.info('Find users with parameters');
        return this.userRepository.supervisorSearch(firstName, lastName, email, username,
                                                    role, size, page);
    }

    public async userSearch(firstName: string,
                            lastName:  string,
                            email:     string,
                            username:  string,
                            role:      string,
                            size:      number,
                            page:      number): Promise<{ data: User[], count: number }> {
        this.log.info('Find users with parameters');
        return this.userRepository.userSearch(firstName, lastName, email, username, role,
                                              size, page);
    }

    public findOne(id: string): Promise<User | undefined> {
        this.log.info('Find one user');
        return this.userRepository.findOne({id, isDeleted: false});
    }

    /* @Deprecated Service */
    public findOneWithTrips(id: string): Promise<User | undefined> {
        this.log.info('Find one user with trips');
        return this.userRepository.findOne({id, isDeleted: false}, {relations: ['trips']});
    }

    public async create(user: User): Promise<User> {
        this.log.info('Create a new user => ', user.toString());
        user.id = uuid.v1();
        if (user.role !== Role.REGULAR) {
            user.role = Role.REGULAR;
        }
        const created = await this.userRepository.save(user);
        this.eventDispatcher.dispatch(events.user.created, created);
        return created;
    }

    public async update(id: string, user: User): Promise<User | undefined> {
        this.log.info('Update a user with id: ', id);
        const current = await this.userRepository.findOne({id, isDeleted: false});
        if (current === undefined) {
            return current;
        }
        if (user.role !== undefined && current.role !== user.role) {
            this.log.warn(`User ${id} is trying change roles through public update`);
            user.role = current.role;
        }
        if (user.password === undefined || user.password === '') {
            current.password = user.password;
        }
        const candidate = UserService.updateAttributes(current, user);
        const updated = await this.userRepository.save(candidate);
        this.eventDispatcher.dispatch(events.user.updated, updated);
        return updated;
    }

    public async delete(id: string): Promise<string> {
        this.log.info('Delete a user');
        await this.userRepository.safeDelete(id);
        return id;

    }

    public async truncate(id: string): Promise<string> {
        this.log.info('Delete a user');
        await this.userRepository.safeDelete(id);
        await this.tripRepository.safeDeleteByUserId(id);
        return id;
    }

    /* @Deprecated Service */
    public graphqlFind(): Promise<User[]> {
        return this.userRepository.find();
    }

}
