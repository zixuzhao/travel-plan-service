import * as express from 'express';
import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import * as JWT from 'jsonwebtoken';

import { User } from '../models/User';
import { UserRepository } from '../repositories/UserRepository';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { env } from '../../env';
import { Jwt } from '../types/response/Jwt';

@Service()
export class AuthService {
    constructor(
        @Logger(__filename) private log: LoggerInterface,
        @OrmRepository() private userRepository: UserRepository
    ) {
    }

    public parseBearerToken(req: express.Request): { token: string } {
        const authorization = req.header('authorization');

        if (authorization && authorization.split(' ')[0] === 'Bearer') {
            this.log.info('Credentials provided by the client');
            return { token: authorization.split(' ')[1] };
        }

        this.log.info('No credentials provided by the client');
        return undefined;
    }

    public async generateCredential(username: string, password: string): Promise<Jwt> {
        const user = await this.userRepository.findOne({
            where: {
                username,
                isDeleted: false,
            },
        });

        if (user === undefined) {
            return undefined;
        }

        if (await User.comparePassword(user, password)) {
            const token = JWT.sign({
                    id: user.id,
                    role: user.role,
                },
                env.jwt.secret,
                {
                    algorithm: 'HS256',
                    expiresIn: '1h',
                });
            return {
                user: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                },
                token,
            };
        }

        return undefined;
    }

    public async validatePassword(id: string,
                                  password: string): Promise<boolean | undefined> {
        const user = await this.userRepository.findOne({id, isDeleted: false});

        if (user === undefined) {
            return undefined;
        }

        return await User.comparePassword(user, password);
    }

    public async validateUser(token: string): Promise<User> {
        return JWT.verify(token, env.jwt.secret,
                          async (error: Error, payload: { id: string, role: string }) => {
            if (error instanceof Error && payload === undefined) {
                return undefined;
            } else {
                return await this.userRepository.findOne(payload.id);
            }
        });
    }
}
